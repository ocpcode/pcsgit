# PCS mirrored git repository

This repository contains snippets of code specific to PCS, an
Anonymous Certification (AC) construction for securing e-assessment
systems according to the General Data Protection Regulation
([GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)).
It extends the certification features of traditional AC systems by
extending privacy-preserving features, such as traceability of the
signatures and unlinkability between issuers. The release of the code
is for reviewing purposes only.


